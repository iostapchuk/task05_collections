package ostapchuk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ostapchuk.model.comparator.task.CompareHandler;
import ostapchuk.model.string.container.SpeedTest;

/**
 * X Study ArrayDeque in detail.
 *
 * X Create a container that encapsulates an array of String, and that only adds Strings and gets Strings,
 * X so that there are no casting issues during use.
 * X If the internal array isn’t big enough for the next add, your container should automatically resize it.
 * X In main( ), compare the performance of your container with an ArrayList holding Strings.
 *
 * Create a class containing two String objects and make it Comparable
 * so that the comparison only cares about the first String.
 * Fill an array and an ArrayList with objects of your class by using a custom generator
 * (eg, which generates pairs of Country-Capital).
 * Demonstrate that sorting works properly.
 * Now make a Comparator that only cares about the second String and demonstrate that sorting works properly.
 * Also perform a binary search using your Comparator.
 *
 * Сreate a Deque class and test it.
 * 
 *
 * Study hashmap in detail - how to redefine hashcode and equals, how the data storage works.
 *
 * Create console menu using enums & map (2 Views).
 *
 * Create simple binary tree map with Generics (implement such methods as “put”, “get”, “remove”, “print” etc.)
 *
 *
 */

public class Application {
    private static final Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String [] args) {
        CompareHandler compareHandler = new CompareHandler();
        compareHandler.start();
    }

    public static void startSpeedCheck() {
        SpeedTest speedTest = new SpeedTest();
        speedTest.testArrayList();
        speedTest.testStringContainer();
    }
}
