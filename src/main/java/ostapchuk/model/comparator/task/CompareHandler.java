package ostapchuk.model.comparator.task;

import ostapchuk.view.TempView;

import java.util.ArrayList;

/**
 * Fill an array and an ArrayList with objects of your class by using a custom generator
 * (eg, which generates pairs of Country-Capital).
 * Demonstrate that sorting works properly.
 */
public class CompareHandler {
    private StringGenerator stringGenerator;
    private ArrayList<StringCompare> stringArray;

    public void start() {
        stringGenerator = new StringGenerator();
        stringGenerator.start();
        stringArray = new ArrayList<>();
        fillArrayList();
        printArrayList("Unsorted ArrayList");
        sortArrayList();
        printArrayList("\n\nSorted ArrayList");
    }

    private void fillArrayList() {
        String[][] countriesAndCapitals = stringGenerator.getCountriesAndCapitals();
        for (int i = 0; i < stringGenerator.getROWS(); i++) {
            StringCompare<StringCompare> temp = new StringCompare<>();
            temp.setObj1(countriesAndCapitals[i][0]);
            temp.setObj2(countriesAndCapitals[i][1]);
            stringArray.add(temp);
        }
    }

    private void sortArrayList() {
        //todo
        while (true) {
            boolean changeIsMade = false;
            for (int i = 0; i < stringArray.size()-1; i++) {
                for (int j = i + 1; j < stringArray.size(); j++) {
                    if (stringArray.get(i).compareTo(stringArray.get(j)) == 0) {
                        continue;
                    } else if (stringArray.get(i).compareTo(stringArray.get(j)) == 1) {
                        changeIsMade = true;
                        String iFirst = stringArray.get(i).getObj1();
                        String iSecond = stringArray.get(i).getObj2();
                        String jFirst = stringArray.get(j).getObj1();
                        String jSecond = stringArray.get(j).getObj2();
                        stringArray.get(i).setObj1(jFirst);
                        stringArray.get(i).setObj2(jSecond);
                        stringArray.get(j).setObj1(iFirst);
                        stringArray.get(j).setObj2(iSecond);
                    }
                }
            }
            if (!changeIsMade) {
                break;
            }
        }
    }

    private void printArrayList(String message) {
        TempView tempView = new TempView();
        tempView.print(message);
        for (int i = 0; i < stringArray.size(); i++) {
            tempView.print(stringArray.get(i).getObj1() + " - " + stringArray.get(i).getObj2());
        }
    }
}
