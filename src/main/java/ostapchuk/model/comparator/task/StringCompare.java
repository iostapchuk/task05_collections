package ostapchuk.model.comparator.task;

/**
 * Create a class containing two String objects and make it Comparable
 * so that the comparison only cares about the first String.
 * Fill an array and an ArrayList with objects of your class by using a custom generator
 * (eg, which generates pairs of Country-Capital).
 * Demonstrate that sorting works properly.
 *
 * Now make a Comparator that only cares about the second String and demonstrate that sorting works properly.
 * Also perform a binary search using your Comparator.
 */

public class StringCompare<T extends StringCompare> implements Comparable<T>{
    private String obj1;
    private String obj2;

    public StringCompare() {

    }

    public StringCompare(String first, String second) {
        this.obj1 = first;
        this.obj2 = second;
    }

    public int compareTo(T t) {
        if(t.getObj1().equals(this.obj1)) {
            return 0;
        }
        char[] obj1Array = obj1.toCharArray();
        char[] tArray = t.getObj1().toCharArray();
        for(int i = 0; i < getSmallerLength(t); i++) {
            if(obj1Array[i] < tArray[i]) {
                return -1;
            } else if(tArray[i] < obj1Array[i]) {
                return 1;
            }
        }
        if(obj1.length() < t.getObj1().length()) {
            return -1;
        } else return  1;
    }

    private int getSmallerLength(T t) {
        if(t.getObj1().length() < obj1.length()) {
            return t.getObj1().length();
        } else return  obj1.length();
    }

    private int getBiggerLength(T t) {
        if(t.getObj1().length() > obj1.length()) {
            return t.getObj1().length();
        } else return  obj1.length();
    }

    public void setObj1(String obj1) {
        this.obj1 = obj1;
    }

    public void setObj2(String obj2) {
        this.obj2 = obj2;
    }

    public String getObj1() {
        return obj1;
    }

    public String getObj2() {
        return obj2;
    }
}
