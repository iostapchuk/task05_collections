package ostapchuk.model.comparator.task;

import java.io.*;

public class StringGenerator {
    private final int ROWS = 18;
    private final int COLUMNS = 2;
    private String[][] countriesAndCapitals = new String[ROWS][COLUMNS];
    private String[] tempString = new String[ROWS];

    public void start() {
        try {
            getFile();
        } catch (Exception e) {
            e.getCause();
        }
        tempStringParser();
    }

    private void getFile() throws Exception {
        String resPath = "/texts/CapitalsAndCountries.txt";
        InputStream in = StringGenerator.class.getResourceAsStream(resPath);
        if (in == null) {
            throw new Exception("resource not found: " + resPath);
        }
        BufferedInputStream bufferedInputStream = new BufferedInputStream(in);
        readFile(bufferedInputStream);
    }

    private void readFile(BufferedInputStream bis) {
        initStringArray(tempString);
        int symbolsCounter = 0; // first 3 symbols in txt file are bad - we will ignore them
        try {
            int temp;
            int i = 0;
            while ((temp = bis.read()) != -1) {
                symbolsCounter++;
                if(symbolsCounter <= 3){ // ignoring those bad symbols
                    continue;
                }
                char ch = (char) temp;
                if (ch == '\r') {
                    continue;
                }
                tempString[i] += "" + ch;
                if (ch == '\n') {
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void tempStringParser() {
        //todo
        initStringArray(countriesAndCapitals);
        for (int i = 0; i < ROWS; i++) {
            char[] row = tempString[i].toCharArray();
            int j = 0;
            int t = 0;
            while(row[t] != '-') {
                countriesAndCapitals[i][j] += row[t];
                t++;
            }
            j = 1;
            t += 2;
            while(row[t] != '\n' & t < row.length) {
                countriesAndCapitals[i][j] += row[t];
                t++;
            }
        }
    }

    public String[][] getCountriesAndCapitals() {
        return countriesAndCapitals;
    }

    private void initStringArray(String[] array) {
        for(int i = 0; i < array.length; i++) {
            array[i] = "";
        }
    }

    private void initStringArray(String[][] array) {
        for (String[] s : array) {
            initStringArray(s);
        }
    }

    public int getROWS() {
        return ROWS;
    }

    public int getCOLUMNS() {
        return COLUMNS;
    }
}

