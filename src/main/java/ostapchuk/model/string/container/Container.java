package ostapchuk.model.string.container;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Create a container that encapsulates an array of String, and that only adds Strings and gets Strings,
 * so that there are no casting issues during use.
 * If the internal array isn’t big enough for the next add, your container should automatically resize it.
 * In main( ), compare the performance of your container with an ArrayList holding Strings.
 */

public class Container {
    private String[] array;
    private final int DEFAULT_SIZE = 16;

    public Container() {
        this.array = new String[DEFAULT_SIZE];
    }

    public Container(int size) {
        this.array = new String[size];
    }

    public Container(Collection<String> col) {
        this.array = new String[col.size() + col.size() / 2];
        Iterator<String> iterator = col.iterator();
        for (int i = 0; i < col.size(); i++) {
            array[i] = iterator.next();
        }
    }

    public Container(String[] strings) {
        this.array = new String[strings.length + strings.length / 2];
        for (int i = 0; i < strings.length; i++) {
            array[i] = strings[i];
        }
    }

    public String get(int index) throws IndexOutOfBoundsException {
        if (index < array.length) {
            return array[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public void remove(int index) {
        array[index] = null;
    }

    public void remove(String toRemove) {
        remove(find(toRemove));
    }

    public void removeAll(String[] toRemove) {
        for (String s : toRemove) {
            remove(s);
        }
    }

    public void removeAll(int[] toRemove) {
        for (int i : toRemove) {
            remove(i);
        }
    }

    public ArrayList<Integer> findAll(String toFind) {
        ArrayList<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(toFind)) {
                indexes.add(i);
            }
        }
        return indexes;
    }

    public int find(String toFind) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(toFind)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Method adds an array of String to the container
     *
     * @param toAdd - strings to add
     * @return - an array of indexes of strings from toAdd because
     * method places every string in first found free space.
     */
    public int[] add(String[] toAdd) {
        int[] stringsIndexes = new int[toAdd.length];
        if (freeSpots() < toAdd.length) {
            makeBigger();
            add(toAdd);
        } else {
            for (int i = 0; i < toAdd.length; i++) {
                stringsIndexes[0] = add(toAdd[i]);
            }
        }
        return stringsIndexes;
    }

    /**
     * @param toAdd - String that is due to be added to the array
     * @return - index of the placed string because
     * method puts string toAdd at the first free spot
     * so it returns index of this string so we can later find it
     */
    public int add(String toAdd) {
        int stringIndex = -1;
        if (freeSpots() == 0) {
            makeBigger();
            add(toAdd);
        } else {
            stringIndex = addInFirstFreeSpot(toAdd);
        }
        return stringIndex;
    }

    private int addInFirstFreeSpot(String toAdd) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                array[i] = toAdd;
                return i;
            }
        }
        return -1;
    }

    private void makeBigger() {
        String[] tempArray = new String[array.length + array.length * 2];
        for (int i = 0; i < array.length; i++) {
            tempArray[i] = array[i];
        }
        array = tempArray;
    }

    private int takenSpots() {
        int count = 0;
        for (String s : array) {
            if (s != null) {
                count++;
            }
        }
        return count;
    }

    private int freeSpots() {
        return array.length - takenSpots();
    }
}
