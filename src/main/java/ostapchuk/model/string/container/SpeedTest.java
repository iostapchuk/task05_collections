package ostapchuk.model.string.container;

import ostapchuk.view.TempView;

import java.util.ArrayList;

public class SpeedTest {
    public void testArrayList() {
        Timer startArrayListTimer = new Timer();
        Timer stopArrayListTimer = new Timer();
        ArrayList<String> arrayList = new ArrayList<>();
        startArrayListTimer.setTime();
        for (int i = 0; i < 100_000; i++) {
            arrayList.add("Skriptonite");
        }
        for (int i = 0; i < 100_000; i++) {
            arrayList.get(i);
        }
        stopArrayListTimer.setTime();
        new TempView().print("Array list started at:   " + startArrayListTimer.getTime());
        new TempView().print("Array list stopped at:   " + stopArrayListTimer.getTime());
        new TempView().print("Total time for ArrayList:  " +
                (stopArrayListTimer.getTime() - startArrayListTimer.getTime()) / 1000);
    }

    public void testStringContainer() {
        Timer startTimer = new Timer();
        Timer stopTimer = new Timer();
        Container container = new Container(100000);
        startTimer.setTime();
        for (int i = 0; i < 100_000; i++) {
            container.add("Skriptonite");
        }
        for (int i = 0; i < 100_000; i++) {
            container.get(i);
        }
        stopTimer.setTime();
        new TempView().print("Container started at:   " + startTimer.getTime());
        new TempView().print("Container stopped at:   " + stopTimer.getTime());
        new TempView().print("Total time for String:  " +
                (stopTimer.getTime() - startTimer.getTime()) / 1000);
    }

}
