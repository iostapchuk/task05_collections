package ostapchuk.model.string.container;

public class Timer {
    private long time;

    public void setTime() {
        this.time = System.nanoTime();
    }

    public long getTime() {
        return time;
    }
}
