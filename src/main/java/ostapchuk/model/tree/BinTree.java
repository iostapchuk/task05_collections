package ostapchuk.model.tree;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/*
Create simple binary tree map with Generics
(implement such methods as “put”, “get”, “remove”, “print” etc.)
імплемент мап, перебіг дерева і всякі інші методи
 */

public class BinTree<K extends Number, V> extends TreeMap implements TreeModel, Map {
    private Node mainRoot;

    public BinTree() {

    }

    public BinTree(K key, V value) {
        putFirst(key, value);
    }

    public int size() {
        return Node.treeSize;
    }

    public boolean isEmpty() {
        if (Node.treeSize > 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean containsKey(K key) {
        if (get(key) == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean containsValue(Object value) {
        return false;
    }

    //TODO
    public Object put(K k, V val) {
        if (isEmpty()) {
            putFirst(k, val);
        } else {
            Node sameKeyNode = get(k);
            if (sameKeyNode != null) {
                Object previousValue = sameKeyNode.getValue();
                sameKeyNode.setValue(val);
                return previousValue;
            } else {
                putNewNode(k, val);
            }
        }
        return null;
    }

    private void putNewNode(K k, V val) {
        Node current = new Node(k, val);
        Node parent = getClosest(k);
        if (current.compareTo(parent) > 0) {
            parent.setRightChild(current);
        } else {
            parent.setLeftChild(current);
        }
        Node.treeSize++;
    }

    private void inOrder(Node localRoot) {
        if (localRoot != null) {
            inOrder(localRoot.getLeftChild());
            //TODO print this to string
            localRoot.toString();
            inOrder(localRoot.getRightChild());
        }
    }

    //TODO
    public Object remove(K k) {
        Node toDelete = get(k);
        if (toDelete == null) {
            return null;
        } else if (toDelete.equals(mainRoot)) {
            mainRoot = null;
        }
        howToDelete(toDelete);


        return null;
    }

    private void howToDelete(Node toDelete) {
        if ((toDelete.getLeftChild() == null) && (toDelete.getRightChild() == null)) {
            deleteLeaf(toDelete);
        } else if ((toDelete.getLeftChild() != null) | (toDelete.getRightChild() != null)) {
            oneOffspringDel(toDelete);
        } else if ((toDelete.getLeftChild() != null) & (toDelete.getRightChild() != null)) {
            twoOfferingsDel(toDelete);
        }
    }

    //TODO
    private void twoOfferingsDel(Node toDelete) {
        Node successor = getSuccessor(toDelete);
        if(successor.compareTo(toDelete) > 0) {
            rightSuccessor(toDelete, successor);
        } else {
            leftSuccessor(toDelete, successor);
        }
    }

    private void rightSuccessor(Node toDelete, Node successor) {
        Node parent = toDelete.getParent();
        successor.setLeftChild(toDelete.getLeftChild());
        parent.setRightChild(successor);
        successor.setParent(parent);
    }

    private void leftSuccessor(Node toDelete, Node successor) {
        //TODO
    }

    private void oneOffspringDel(Node toDelete) {
        Node parent = toDelete.getParent();
        Node grandChild = getOnlyChild(toDelete);
        if (isLeftOrRightChild(toDelete) == LeftOrRight.RIGHT) {
            parent.setRightChild(grandChild);
            grandChild.setParent(parent);
        } else {
            parent.setLeftChild(grandChild);
            grandChild.setParent(parent);
        }
    }

    private void deleteLeaf(Node toDelete) {
        Node parent = toDelete.getParent();
        if (isLeftOrRightChild(toDelete) == LeftOrRight.RIGHT) {
            parent.setRightChild(null);
        } else if (isLeftOrRightChild(toDelete) == LeftOrRight.LEFT) {
            parent.setLeftChild(null);
        }
    }

    private Node getOnlyChild(Node node) {
        if (node.getRightChild() != null) {
            return node.getRightChild();
        } else
            return node.getLeftChild();
    }

    private LeftOrRight isLeftOrRightChild(Node node) {
        Node parent = node.getParent();
        if (parent.getRightChild().equals(node)) {
            return LeftOrRight.RIGHT;
        } else {
            return LeftOrRight.LEFT;
        }
    }

    private Node getSuccessor(Node toDelete) {
        Node successorParent = toDelete;
        Node successor = toDelete;
        Node current = toDelete.getRightChild(); // Переход к правому потомку
        while (current != null) // Пока остаются левые потомки
        {
            successorParent = successor;
            successor = current;
            current = current.getLeftChild(); // Переход к левому потомку
        }
// Если преемник не является
        if (successor != toDelete.getRightChild()) // правым потомком,
        { // создать связи между узлами
            successorParent.setLeftChild(successor.getRightChild());
            successor.setRightChild(toDelete.getRightChild());
        }
        return successor;
    }

    public String print(K key) {
        return getClosest(key).toString();
    }

    public void clear() {
        mainRoot = null;
    }

    /**
     * Finds the Node with the key value same as K key.
     * If there is not such key in the tree method will return Node
     * with the key that is the closest to the parameter K key.
     * All keys are compared by their double value
     */
    private Node getClosest(K key) {
        Node current = mainRoot;
        Node needed = new Node(key, null);
        while (needed.compareTo(current) != 0) {
            if (needed.compareTo(current) < 0) {
                current = current.getLeftChild();
            } else {
                current = current.getRightChild();
            }
            if (current == null) {
                //TODO say that this is the closest Node
                return current.getParent();
            }
        }
        return current;
    }

    private Node get(K key) {
        Node current = mainRoot;
        Node needed = new Node(key, null);
        while (needed.compareTo(current) != 0) {
            if (needed.compareTo(current) < 0) {
                current = current.getLeftChild();
            } else {
                current = current.getRightChild();
            }
            if (current == null) {
                return null;
            }
        }
        return current;
    }

    private void putFirst(K key, V value) {
        mainRoot = new Node(key, value);
        Node.treeSize++;
    }


    public Set keySet() {
        return null;
    }

    public Collection values() {
        return null;
    }

    public Set<Entry> entrySet() {
        return null;
    }

    public void putAll(Map m) {

    }
}
