package ostapchuk.model.tree;

public enum LeftOrRight {
    LEFT, RIGHT
}
