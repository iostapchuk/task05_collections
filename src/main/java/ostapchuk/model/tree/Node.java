package ostapchuk.model.tree;

public class Node<K extends Number, V, T extends Node> implements Comparable<T> {
    private Node parent;
    private Node leftChild;
    private Node rightChild;
    private K key;
    private V value;
    int level;
    static int treeSize = 0;

    Node(Node par, Node left, Node right) {
        this.setParent(par);
        this.setLeftChild(left);
        this.setRightChild(right);
        treeSize++;
    }

    Node(K k, V v) {
        this.setKey((K) new Integer(k.intValue()));
        this.setValue(v);
    }

    public int compareTo(T node) {
        if ((this.getKey().intValue()) == node.getKey().intValue()) {
            return 0;
        } else {
            return (int) (this.getKey().intValue() - node.getKey().intValue());
        }
    }

    @Override
    public String toString() {
            return ("Node`s key is: " + this.getKey() + ".\n" +
                    "And it's value is: " + this.getValue());
    }

    public boolean equals(Object obj) {
        if(this.hashCode() == obj.hashCode()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        String toConvert = "" + this.getKey() + "/" + this.getValue() + "";
        char[] convertToInt = toConvert.toCharArray();
        int hash = 0;
        for(int i : convertToInt) {
            hash += (int) i;
        }
        return hash;
    }

    /**
     * Field are not private as it is not really needed.
     * Writing public getters and setters with no control
     * will not add anything good, it will just make code less readable.
     * But fields are package private, so they are encapsulated
     * in this package so neither classes from view nor classes
     * from controller packages will see them.
     */
    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
