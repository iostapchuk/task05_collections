package ostapchuk.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ostapchuk.Application;

public class TempView {
    private static final Logger logger1 = LogManager.getLogger(Application.class);

    public void print(String s) {
        logger1.info(s);
    }
}
